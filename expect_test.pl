#!/usr/bin/perl

# Copyright (c) 2020-2024 Philip Hands <phil@hands.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;
use Expect;
use Getopt::Std;
use Test2::V0;

my %opt=();
getopts("cfPh:i:p:R:st:u:xE:", \%opt) and defined($opt{h}) and defined($opt{u}) or
    die "Usage: [-f] [-s] [-x] [-c] [-P] [-E] [-u <user>] [-p <password>] [-i <id_file>] [-t target_path] [-R ro_user] -h <host>\n";

my $user_host = "$opt{u}\@$opt{h}";

my $ro_user_host;
$ro_user_host = "$opt{R}\@$opt{h}" if defined($opt{R});

my $timeout = 20 ;

my %idx;

sub test_ssh_copy_id {
  my $command;

  if (defined($opt{c})) {
    $command = 'KCOV_1TO8=1 /usr/bin/kcov --bash-dont-parse-binary-dir /tmp/kcov ' . $_[0] . ' 8>&1' ;
  } else {
    $command = $_[0];
  }

  # add environemnt settings, if specified as 2nd param
  if (defined $_[1]) {
    $command = $_[1] . " " . $command ;
  }

  # ADD more ENV bits here, via -E option $opt{E}
  if (defined($opt{E})) {
    $command = $opt{E} . " " . $command ;
  }

  my $exp = Expect->new;
  $exp->raw_pty(1);
  printf "about to run '%s'\n", $command ;
  $exp->spawn($command)
    or die "Cannot spawn ssh-copy-id: $!\n";

  my $spawn_ok = my $viable_found = 0;
  my ($matched_pattern_position, $error, $successfully_matching_string, $before_match,
      $after_match) = "";
  my @match_patterns = (
    ['INFO: ', sub {$spawn_ok = 1; exp_continue;}],
    ['assword: ', sub {my $self = shift; $self->send("$opt{p}\n") ; exp_continue} ]
   );
  my $i = 3;
  push(@match_patterns,(['This service allows sftp connections only.', sub {note("    found: sftp only")}]));
  $idx{SFTP_ONLY} = $i++;
  push(@match_patterns,(['Would have added the following key\(s\):', sub {note("    found: would have added (-n - dry run)")}]));
  $idx{DRY_RUN} = $i++;
  push(@match_patterns,(['Number of key\(s\) added:', sub {note("    found: keys added")}]));
  $idx{KEYS_ADDED} = $i++;
  push(@match_patterns,(['WARNING: All keys were skipped', sub {note("    found: All skipped")}]));
  $idx{KEYS_SKIPPED} = $i++;
  push(@match_patterns,(['ERROR: Less dimwitted shell required.', sub {note("    found: dimwitted")}]));
  $idx{DIMWIT} = $i++;
  push(@match_patterns,(['seems viable', sub { $viable_found = 1; note("    found: seems viable") ; exp_continue}]));
  $idx{VIABLE} = $i++;
  push(@match_patterns,(['ERROR: Too many arguments.', sub {note("    found: Too many arguments")}]));
  $idx{XS_ARGS} = $i++;
  push(@match_patterns,(['ERROR: -i option must not be ', sub {note("    found: Too many -i options")}]));
  $idx{XS_I_OPTS} = $i++;
  push(@match_patterns,(['ERROR: failed to open ID file', sub {note("    found: failed ... ID file")}]));
  $idx{MISSING_ID_FILE} = $i++;
  push(@match_patterns,(['ERROR: No identities found', sub {note("    found: No identities")}]));
  $idx{NO_IDS} = $i++;
  push(@match_patterns,(['Connection refused', sub {note("    found: Connection refused")}]));
  $idx{CONN_REF} = $i++;
  push(@match_patterns,(['ERROR: Missing hostname.', sub {note("    found: Missing hostname")}]));
  $idx{MISSING_HOST} = $i++;
  push(@match_patterns,(['Permission denied', sub {note("    found: Permission denied")}]));
  $idx{EPERM} = $i++;
  push(@match_patterns,(['[+] getopts', sub {note("    found: set -x output")}]));
  $idx{SET_X_NOISE} = $i++;
  push(@match_patterns,(['Usage: ', sub {note("    found: Usage")}]));
  $idx{USAGE} = $i++;
  push(@match_patterns,(['ssh: .* Name or service not known', sub {note("    found: Name not known")}]));
  $idx{UNK_HOST} = $i++;
  push(@match_patterns,([ eof =>
                          sub {
                            if ($spawn_ok) {
                              diag("ERROR: premature EOF while running ssh-copy-id.");
                            } else {
                              diag("ERROR: could not spawn ssh-copy-id.");
                            }
                          }
                         ]));
  $idx{EOF} = $i++;
  push(@match_patterns,([ timeout =>
                          sub {
                            diag("timeout waiting for ssh-copy-id to produce output");
                            return -1;
                          }
                         ]));
  $idx{TIMEOUT} = $i++;


  ($matched_pattern_position, $error, $successfully_matching_string,
   $before_match, $after_match) =
      $exp->expect($timeout, @match_patterns);

  note ("result: ", $matched_pattern_position, " match [", $successfully_matching_string, "] after match [", $after_match, "] error [", $error, "]") ;
  $exp->soft_close() ;
  $matched_pattern_position = 99 unless $matched_pattern_position > 0;
  return $matched_pattern_position + 100 * $viable_found;
}

sub broken_opt_i {
  my $args = shift;
  if ($args =~ /^(.*-i\s*)(.*)$/) {
    return $1 . "this/should/break/things" . $2;
  } else {
    return $args . " -i this/should/break/things";
  }
}

sub broken_opt_p {
  my $args = shift;
  if ($args =~ /^(.*-p)[0-8](.*)$/) {
    return $1 . "9" . $2;
  } else {
    return $args . " -p9876";
  }
}

my $sftp_flag = my $sftp_comment = my $extra_args = '' ;

my $ssh_copy_id = './ssh-copy-id ';

if (defined($opt{s})) {
  $sftp_flag = '-s ' ;
  $sftp_comment = '(sftp)';
}

$extra_args .= "-x " if (defined($opt{x})) ;
$extra_args .= "-f " if (defined($opt{f})) ;
$extra_args .= "-i $opt{i} " if (defined($opt{i})) ;
# this allows one to pass extra options through to the ssh/sftp calls
$extra_args .= $ARGV[0] if defined($ARGV[0]) ;

is( test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' -n ' . $ro_user_host),
    defined($opt{f}) ? $idx{DRY_RUN} : $idx{KEYS_SKIPPED},
    defined($opt{f})
    ? "Says 'Would have ...' because -n, but readonly, so would fail $sftp_comment"
    : "All keys skipped $sftp_comment (sshreadonly)"
  )
  if defined($opt{R});

SKIP: {
  skip "cannot provoke failure, as not an SFTP user", 1 unless defined($opt{s});

  is(test_ssh_copy_id($ssh_copy_id . $extra_args . ' ' . $user_host),    $idx{SFTP_ONLY}, "get rejected ssh login when SFTP only");
}

is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' -n ' . $user_host), $idx{DRY_RUN}, "keys not added [dry run] $sftp_comment");

if (defined($opt{t})) {
  # if we have -t, install key(s) to the wrong place first, since we won't thus stop the normal install
  is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . " -t '$opt{t}'" . ' ' . $user_host), $idx{KEYS_ADDED}, "keys added [-t $opt{t}] $sftp_comment");
}
is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $user_host), $idx{KEYS_ADDED}, "keys added $sftp_comment");

if (defined($opt{R}) && defined($opt{i})) {
  # if we have -i & -R that should involve an attempt to install new key(s) to a readonly file which will fail
  is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $ro_user_host), $idx{EPERM}, "readonly user provokes 'Permission denied' $sftp_comment (sshreadonly)");
}

SKIP: {
  skip "-f specified, so duplicate key test is meaningless", 1 if defined($opt{f});

  is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $user_host), $idx{KEYS_SKIPPED}, "All keys skipped $sftp_comment");
}


# 2 args, gives usage
is(test_ssh_copy_id($ssh_copy_id . $extra_args . " foo bar"), $idx{XS_ARGS}, "too many arguments");

# 2 -i's, gives usage
is(test_ssh_copy_id($ssh_copy_id . $extra_args . " -i -i " . $user_host), $idx{XS_I_OPTS}, "multiple -i options");

# -i to a non-existant path
is(test_ssh_copy_id($ssh_copy_id . broken_opt_i($extra_args) . " " . $user_host), $idx{MISSING_ID_FILE}, "missing ID file");

is(test_ssh_copy_id($ssh_copy_id . '-x -h'), defined($opt{c}) ? $idx{USAGE} : $idx{SET_X_NOISE}, "-x gives debug output (or not when under kcov, which eats STDERR)");

if (defined($opt{i}) && ! defined($ARGV[0])) {
  # missing host (with -i <id_file> as the last option, which gets a special case to avoid useless messages about lookup failures)
  is(test_ssh_copy_id($ssh_copy_id . $extra_args), $idx{MISSING_HOST}, "missing host");
}
else {
  # missing host with no -i option, gives usage
  is(test_ssh_copy_id($ssh_copy_id . $extra_args), $idx{USAGE}, "missing host -> Usage");
}

# specify a nonexistent host
is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' ' . $opt{u} . '@this.is.a.nonexistent.host.example.com'), $idx{UNK_HOST}, "Nonexistent hostname");

# specify the wrong port
is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . broken_opt_p($extra_args) . ' ' . $user_host), $idx{CONN_REF}, "wrong port - connection refused");

sub printf_to_file {
  my $file = shift;
  open(my $fh, ">", "$file") or die "Can't open > '$file' : $!";
  printf $fh @_;
  close($fh) or die "Can't close '$file' : $!";
}
printf_to_file(".ssh/id_empty.pub", '');
printf_to_file(".ssh/id_empty", '');
is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . " -i " . $user_host), $idx{NO_IDS}, "empty key - no identities found");
unlink(".ssh/id_empty.pub", ".ssh/id_empty");

if (defined($opt{P})) {
  is(test_ssh_copy_id($ssh_copy_id . '-h', "FAKE_FAIL=1" ), $idx{DIMWIT}, "dimwitted shell (when false=true)");
  is(test_ssh_copy_id($ssh_copy_id . '-h', "FAKE_FAIL=1 SANE_SH=/bin/dash"), $idx{USAGE}+100, "seems viable (when false=true) & Usage");
}

if (!defined($opt{f})) {
  # test the use of an alternative ssh config
  printf_to_file("other_ssh_config", "Host alias_for_localhost\n\tHostname localhost\n");
  # system("( pwd ; head -v other_ssh_config ; ) >&2");
  is(test_ssh_copy_id($ssh_copy_id . $sftp_flag . $extra_args . ' -F other_ssh_config ' . $opt{u} . '@alias_for_localhost'),
     $idx{KEYS_SKIPPED}, "All keys skipped $sftp_comment");
  unlink("other_ssh_config");
}

done_testing;
